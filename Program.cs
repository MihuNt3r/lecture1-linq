﻿using System;
using System.Net.Http;
using System.Collections.Generic;
using Lecture1_LINQ_.Entities;
using System.Threading.Tasks;

namespace Lecture1_LINQ_
{
    class Program
    {
        static async System.Threading.Tasks.Task Main(string[] args)
        {
            Menu menu = new Menu();
            await menu.Start();
        }
    }
}
