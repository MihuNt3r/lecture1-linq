﻿using System;
using System.Net.Http;
using System.Net.Http.Json;
using System.Net.Http.Headers;
using Lecture1_LINQ_.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Lecture1_LINQ_
{
    public class HttpHandler
    {
        private readonly HttpClient _client;

        public HttpHandler()
        {
            _client = new HttpClient();
            _client.BaseAddress = new Uri("https://bsa21.azurewebsites.net/");
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<List<ProjectDTO>> GetProjects()
        {
            var action = "api/projects";

            HttpResponseMessage response = await _client.GetAsync(action);
            if (response.IsSuccessStatusCode)
            {
                var projects = await response.Content.ReadFromJsonAsync<List<ProjectDTO>>();
                return projects;
            }

            return null;
        }

        public async Task<ProjectDTO> GetProjectById(int id)
        {
            var action = $"api/projects/{id}";

            HttpResponseMessage response = await _client.GetAsync(action);
            if (response.IsSuccessStatusCode)
            {
                var project = await response.Content.ReadFromJsonAsync<ProjectDTO>();
                return project;
            }

            return null;
        }

        public async Task<List<DTOs.TaskDTO>> GetTasks()
        {
            var action = "api/tasks";

            HttpResponseMessage response = await _client.GetAsync(action);
            if (response.IsSuccessStatusCode)
            {
                var tasks = await response.Content.ReadFromJsonAsync<List<DTOs.TaskDTO>>();
                return tasks;
            }

            return null;
        }

        public async Task<DTOs.TaskDTO> GetTaskById(int id)
        {
            var action = $"api/tasks/{id}";

            HttpResponseMessage response = await _client.GetAsync(action);
            if (response.IsSuccessStatusCode)
            {
                var task = await response.Content.ReadFromJsonAsync<DTOs.TaskDTO>();
                return task;
            }

            return null;
        }

        public async Task<List<TeamDTO>> GetTeams()
        {
            var action = "api/teams";

            HttpResponseMessage response = await _client.GetAsync(action);
            if (response.IsSuccessStatusCode)
            {
                var teams = await response.Content.ReadFromJsonAsync<List<TeamDTO>>();
                return teams;
            }

            return null;
        }

        public async Task<TeamDTO> GetTeamById(int id)
        {
            var action = $"api/teams/{id}";

            HttpResponseMessage response = await _client.GetAsync(action);
            if (response.IsSuccessStatusCode)
            {
                var team = await response.Content.ReadFromJsonAsync<TeamDTO>();
                return team;
            }

            return null;
        }

        public async Task<List<UserDTO>> GetUsers()
        {
            var action = "api/users";

            HttpResponseMessage response = await _client.GetAsync(action);
            if (response.IsSuccessStatusCode)
            {
                var users = await response.Content.ReadFromJsonAsync<List<UserDTO>>();
                return users;
            }

            return null;
        }

        public async Task<UserDTO> GetUserById(int id)
        {
            var action = $"api/users/{id}";

            HttpResponseMessage response = await _client.GetAsync(action);
            if (response.IsSuccessStatusCode)
            {
                var user = await response.Content.ReadFromJsonAsync<UserDTO>();
                return user;
            }

            return null;
        }
    }
}
