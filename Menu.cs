﻿using System;
using System.Threading.Tasks;
using Lecture1_LINQ_.Entities;
using System.Collections.Generic;

namespace Lecture1_LINQ_
{
    public class Menu
    {
        private readonly QueryHandler _handler = new QueryHandler();

        public async System.Threading.Tasks.Task Start()
        {
            int a = 0;
            while (a != 8)
            {
                Console.WriteLine("Available commands:\n" +
                    "1.Get Dictionary <Project, TasksCount> by user id\n" +
                    "2.Get list of tasks assigned to user by id(Task name < 45 symbols)\n" +
                    "3.Get list of tasks finished in 2021 by user id\n" +
                    "4.Get list sorted by Registation date (Id, Team name, Users) where users are older than 10 years\n" +
                    "5.Get list of users sorted by first name with tasks sorted by name lenght\n" +
                    "6.Get info about User, his last project, not finished tasks and longest task\n" +
                    "7.Get info about Project, longest task(by description), shortest task(by name), count of users in team\n" +
                    "8.Exit");
                int cmd = 0;
                try
                {
                    cmd = Int32.Parse(Console.ReadLine());
                }
                catch (Exception exc)
                {
                    Console.WriteLine(exc.Message);
                }

                switch (cmd)
                {
                    case 1:
                        Console.WriteLine("Enter id of a user");
                        try
                        {
                            int id = Int32.Parse(Console.ReadLine());
                            Dictionary<Project, int> dict = await _handler.GetTasksAmountByUserId(id);
                            if (dict.Count > 0)
                            {
                                foreach (var item in dict)
                                {
                                    Console.WriteLine(String.Format("Name: {0,-50} | Tasks: {1,-10}", item.Key.Name, item.Value));
                                }
                            }
                            else
                            {
                                Console.WriteLine("No results");
                            }
                        }
                        catch (Exception exc)
                        {
                            Console.WriteLine(exc.Message);
                        }
                        break;
                    case 2:
                        Console.WriteLine("Enter id of a user");
                        try
                        {
                            int id = Int32.Parse(Console.ReadLine());
                            var tasks = await _handler.GetListOfTasksByUserId(id);
                            if (tasks.Count > 0)
                            {
                                Console.WriteLine("List of tasks:");
                                foreach (var t in tasks)
                                {
                                    Console.WriteLine("Name: " + t.Name);
                                }
                            }
                            else
                            {
                                Console.WriteLine("No results");
                            }
                        }
                        catch (Exception exc)
                        {
                            Console.WriteLine(exc.Message);
                        }
                        break;
                    case 3:
                        Console.WriteLine("Enter id of a user");
                        try
                        {
                            int id = Int32.Parse(Console.ReadLine());
                            var tasks = await _handler.GetTasksFinishedIn2021(id);
                            if (tasks.Count > 0)
                            {
                                Console.WriteLine("List of finished tasks:");
                                foreach (var t in tasks)
                                {
                                    Console.WriteLine("Name: " + t.Name);
                                }
                            }
                            else
                            {
                                Console.WriteLine("No results");
                            }
                        }
                        catch (Exception exc)
                        {
                            Console.WriteLine(exc.Message);
                        }
                        break;
                    case 4:
                        try
                        {
                            var teams = await _handler.Task4Method();
                            if (teams.Count > 0)
                            {
                                Console.WriteLine("List of finished tasks:");
                                foreach (var t in teams)
                                {
                                    Console.WriteLine(String.Format("Id: {0,-5} | Name: {1}", t.Id, t.Name));
                                    foreach (var u in t.Users)
                                    {
                                        Console.WriteLine(String.Format("LastName: {0,-10} | Registation Date: {1}", u.LastName, u.RegisteredAt.ToString()));
                                    }
                                }
                            }
                            else
                            {
                                Console.WriteLine("No results");
                            }
                        }
                        catch (Exception exc)
                        {
                            Console.WriteLine(exc.Message);
                        }
                        break;
                    case 5:
                        try
                        {
                            var users = await _handler.GetSortedUsers();
                            if (users.Count > 0)
                            {
                                foreach (var u in users)
                                {
                                    Console.WriteLine("User:");
                                    Console.WriteLine(String.Format("Id: {0,-5} | Name: {1}", u.Id, u.FirstName));
                                    Console.WriteLine("Tasks");
                                    foreach (var t in u.Tasks)
                                    {
                                        Console.WriteLine($"Name: {t.Name}");
                                    }
                                }
                            }
                            else
                            {
                                Console.WriteLine("No results");
                            }
                        }
                        catch (Exception exc)
                        {
                            Console.WriteLine(exc.Message);
                        }
                        break;
                    case 6:
                        Console.WriteLine("Enter id of a user");
                        try
                        {
                            int id = Int32.Parse(Console.ReadLine());
                            var entity = await _handler.Task6Method(id);
                            if (entity != null)
                            {
                                Console.WriteLine(String.Format("User: {0} {1}", entity.User.FirstName, entity.User.LastName));
                                Console.WriteLine(String.Format("Last project: {0}", entity.LastProject.Name));
                                Console.WriteLine(String.Format("Tasks in the last project: {0}", entity.LastProjectTasks));
                                Console.WriteLine(String.Format("Total not finished tasks: {0}", entity.NotFinishedTasksCount));
                                Console.WriteLine(String.Format("Task with longest period: {0}", entity.TaskWithLongestPeriod.Name));
                            }
                            else
                            {
                                Console.WriteLine("No results");
                            }
                        }
                        catch (Exception exc)
                        {
                            Console.WriteLine(exc.Message);
                        }
                        break;
                    case 7:
                        try
                        {
                            var projects = await _handler.Task7Method();
                            if (projects.Count > 0)
                            {
                                foreach (var p in projects)
                                {
                                    Console.WriteLine(String.Format("Project: {0}", p.Project.Name));
                                    Console.WriteLine(String.Format("Task with longest description: {0}", p.LongestTaskByDescription != null ? 
                                        p.LongestTaskByDescription.Name : "No result"));
                                    Console.WriteLine(String.Format("Task with shortest name: {0}", p.ShortestTaskByName != null ?
                                        p.ShortestTaskByName.Name : "No result"));
                                    Console.WriteLine(String.Format("Total users count: {0}", p.CustomersCount));
                                }
                            }
                            else
                            {
                                Console.WriteLine("No results");
                            }
                        }
                        catch (Exception exc)
                        {
                            Console.WriteLine(exc.Message);
                        }
                        break;
                    case 8:
                        a = 8;
                        break;
                    default:
                        Console.WriteLine("Error. Try again");
                        break;

                }
            }

            
        }
    }
}
