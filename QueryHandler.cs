﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lecture1_LINQ_.Entities;
using Lecture1_LINQ_.DTOs;

namespace Lecture1_LINQ_
{
    public class QueryHandler
    {
        private HttpHandler _handler = new HttpHandler();

        /// <summary>
        /// Method takes all DTOs and returns hierarchy of elements
        /// </summary>
        public void CreateHierarchy(List<TeamDTO> teamDtos, List<UserDTO> userDtos, 
            List<ProjectDTO> projectDtos, List<TaskDTO> taskDtos,
            out List<Team> teams, out List<User> users,
            out List<Project> projects, out List<Entities.Task> tasks)
        {
            teams = (from t in teamDtos
                        select new Team
                        {
                            Id = t.Id,
                            Name = t.Name,
                            CreatedAt = t.CreatedAt
                        }).ToList();

            users = (from u in userDtos
                     join t in teams
                     on u.TeamId equals t.Id
                     select new User
                     {
                         Id = u.Id,
                         Team = t,
                         FirstName = u.FirstName,
                         LastName = u.LastName,
                         Email = u.Email,
                         RegisteredAt = u.RegisteredAt,
                         BirthDay = u.BirthDay
                     }).ToList();


            projects = (from p in projectDtos
                        join t in teams
                        on p.TeamId equals t.Id
                        join u in users
                        on p.AuthorId equals u.Id
                        select new Project
                        {
                            Id = p.Id,
                            Author = u,
                            Team = t,
                            Name = p.Name,
                            Description = p.Description,
                            Deadline = p.Deadline
                        }).ToList();

            tasks = (from t in taskDtos
                     join p in projects
                     on t.ProjectId equals p.Id
                     join u in users
                     on t.PerformerId equals u.Id
                     select new Entities.Task
                     {
                         Id = t.Id,
                         Performer = u,
                         Project = p,
                         Name = t.Name,
                         Description = t.Description,
                         State = t.State,
                         CreatedAt = t.CreatedAt,
                         FinishedAt = t.FinishedAt
                     }).ToList();

            tasks.ForEach(t => t.Project.Tasks.Add(t));
            tasks.ForEach(t => t.Performer.Tasks.Add(t));
            users.ForEach(u => u.Team.Customers.Add(u));
        }

        #region 1-st Task
        public async Task<Dictionary<Project, int>> GetTasksAmountByUserId(int id)
        {
            var user = await _handler.GetUserById(id);
            if (user == null)
                throw new Exception("Can't find user with such id");

            var taskDtos = await _handler.GetTasks();
            var userDtos = await _handler.GetUsers();
            var projectDtos = await _handler.GetProjects();
            var teamDtos = await _handler.GetTeams();

            List<Team> teams;
            List<User> users;
            List<Project> projects;
            List<Entities.Task> tasks;

            CreateHierarchy(teamDtos, userDtos, projectDtos, taskDtos,
                out teams, out users, out projects, out tasks);


            var query = (from t in tasks
                        where t.Performer.Id == id
                        select new
                        {
                            Proj = t.Project,
                            TasksCount = t.Project.Tasks.Count
                        }).Distinct();

            var dict = query.ToDictionary(t => t.Proj, t => t.TasksCount);

            return dict;
        }
        #endregion

        #region 2-nd Task
        public async Task<List<Entities.Task>> GetListOfTasksByUserId(int id)
        {
            var user = await _handler.GetUserById(id);
            if (user == null)
                throw new Exception("Can't find user with such id");

            var taskDtos = await _handler.GetTasks();
            var userDtos = await _handler.GetUsers();
            var projectDtos = await _handler.GetProjects();
            var teamDtos = await _handler.GetTeams();

            List<Team> teams;
            List<User> users;
            List<Project> projects;
            List<Entities.Task> tasks;

            CreateHierarchy(teamDtos, userDtos, projectDtos, taskDtos,
                out teams, out users, out projects, out tasks);


            var query = from t in tasks
                        where t.Performer.Id == id &&
                        t.Name.Length < 45
                        select t;

            var list = query.ToList();


            return list;
        }
        #endregion

        #region 3-rd Task
        public async Task<List<Task3Entity>> GetTasksFinishedIn2021(int id)
        {
            var user = await _handler.GetUserById(id);
            if (user == null)
                throw new Exception("Can't find user with such id");

            var taskDtos = await _handler.GetTasks();
            var userDtos = await _handler.GetUsers();
            var projectDtos = await _handler.GetProjects();
            var teamDtos = await _handler.GetTeams();

            List<Team> teams;
            List<User> users;
            List<Project> projects;
            List<Entities.Task> tasks;

            CreateHierarchy(teamDtos, userDtos, projectDtos, taskDtos,
                out teams, out users, out projects, out tasks);


            var query = from t in tasks
                        where t.Performer.Id == id
                        where t.FinishedAt != null &&
                        t.FinishedAt.Value.Year == 2021
                        select new Task3Entity
                        {
                            Id = t.Id,
                            Name = t.Name
                        };

            var list = query.ToList();

            return list;
        }
        #endregion

        #region 4-th Task
        public async Task<List<Task4Entity>> Task4Method()
        {
            var taskDtos = await _handler.GetTasks();
            var userDtos = await _handler.GetUsers();
            var projectDtos = await _handler.GetProjects();
            var teamDtos = await _handler.GetTeams();

            List<Team> teams;
            List<User> users;
            List<Project> projects;
            List<Entities.Task> tasks;

            CreateHierarchy(teamDtos, userDtos, projectDtos, taskDtos,
                out teams, out users, out projects, out tasks);


            var query = from t in teams
                        select new Task4Entity
                        {
                            Id = t.Id,
                            Name = t.Name,
                            Users = (from u in users
                                     where u.BirthDay.Year < 2011 &&
                                     u.Team.Id == t.Id
                                     orderby u.RegisteredAt descending
                                     select u).ToList()
                        };

            var list = query.Distinct().ToList();

            return list;
        }
        #endregion

        #region 5-th Task
        public async Task<List<User>> GetSortedUsers()
        {
            var taskDtos = await _handler.GetTasks();
            var userDtos = await _handler.GetUsers();
            var projectDtos = await _handler.GetProjects();
            var teamDtos = await _handler.GetTeams();

            List<Team> teams;
            List<User> users;
            List<Project> projects;
            List<Entities.Task> tasks;

            CreateHierarchy(teamDtos, userDtos, projectDtos, taskDtos,
                out teams, out users, out projects, out tasks);


            var query = from u in users
                        orderby u.FirstName
                        select new User
                        {
                            Id = u.Id,
                            Team = u.Team,
                            FirstName = u.FirstName,
                            LastName = u.LastName,
                            Email = u.Email,
                            RegisteredAt = u.RegisteredAt,
                            BirthDay = u.BirthDay,
                            Tasks = (from t in u.Tasks
                                     orderby t.Name.Length descending
                                     select t).ToList()
                        };


            var list = query.ToList();

            return list;
        }
        #endregion

        #region 6-th Task
        public async Task<Task6Entity> Task6Method(int id)
        {
            var user = await _handler.GetUserById(id);
            if (user == null)
                throw new Exception("Can't find user with such id");

            var taskDtos = await _handler.GetTasks();
            var userDtos = await _handler.GetUsers();
            var projectDtos = await _handler.GetProjects();
            var teamDtos = await _handler.GetTeams();

            List<Team> teams;
            List<User> users;
            List<Project> projects;
            List<Entities.Task> tasks;

            CreateHierarchy(teamDtos, userDtos, projectDtos, taskDtos,
                out teams, out users, out projects, out tasks);


            var query = (from u in users
                        where u.Id == id
                        select new Task6Entity
                        {
                            User = u,
                            LastProject = u.Tasks.Aggregate((curLast, x) => curLast.CreatedAt < x.CreatedAt ? x : curLast).Project,

                            LastProjectTasks = u.Tasks.Aggregate((curLast, x) => curLast.CreatedAt < x.CreatedAt ? x : curLast).Project.Tasks.Count,

                            NotFinishedTasksCount = (from t in u.Tasks
                                                     where t.FinishedAt == null
                                                     select t).ToList().Count,

                            TaskWithLongestPeriod = u.Tasks.Aggregate((curLongst, x) => 
                            (((curLongst.FinishedAt ?? DateTime.Now) - curLongst.CreatedAt) < ((x.FinishedAt ?? DateTime.Now) - x.CreatedAt)) ? 
                            x : curLongst)

                        }).FirstOrDefault();

            return query;
        }
        #endregion

        #region 7-th Task
        public async Task<List<Task7Entity>> Task7Method()
        {
            var taskDtos = await _handler.GetTasks();
            var userDtos = await _handler.GetUsers();
            var projectDtos = await _handler.GetProjects();
            var teamDtos = await _handler.GetTeams();

            List<Team> teams;
            List<User> users;
            List<Project> projects;
            List<Entities.Task> tasks;

            CreateHierarchy(teamDtos, userDtos, projectDtos, taskDtos,
                out teams, out users, out projects, out tasks);

            
            var query = from p in projects
                        select new Task7Entity
                        {
                            Project = p,
                            LongestTaskByDescription = p.Tasks.Count > 0 ? 
                            p.Tasks.Aggregate((curMax, x) => curMax.Description.Length < x.Description.Length ? x : curMax) : null,

                            ShortestTaskByName = p.Tasks.Count > 0 ?
                            p.Tasks.Aggregate((curMin, x) => curMin.Name.Length > x.Name.Length ? x : curMin) : null,

                            CustomersCount = p.Description.Length > 20 || p.Tasks.Count < 3 ?
                            p.Team.Customers.Count : 0
                        };

            var list = query.ToList();

            return list;
        }
        #endregion
    }
}
