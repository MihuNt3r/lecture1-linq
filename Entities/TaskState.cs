﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lecture1_LINQ_.Entities
{
    public enum TaskState
    {
        Started,
        Ongoing,
        Scheduled,
        Finished
    }
}
