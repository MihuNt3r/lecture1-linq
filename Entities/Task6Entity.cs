﻿namespace Lecture1_LINQ_.Entities
{
    public class Task6Entity
    {
        public User User { get; set; }
        public Project LastProject { get; set; }
        public int LastProjectTasks { get; set; }
        public int NotFinishedTasksCount { get; set; }
        public Task TaskWithLongestPeriod { get; set; }
    }
}
