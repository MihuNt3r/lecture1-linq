﻿namespace Lecture1_LINQ_.Entities
{
    public class Task7Entity
    {
        public Project Project { get; set; }
        public Task LongestTaskByDescription { get; set; }
        public Task ShortestTaskByName { get; set; }
        public int CustomersCount { get; set; }

    }
}
