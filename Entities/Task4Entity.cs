﻿using System.Collections.Generic;

namespace Lecture1_LINQ_.Entities
{
    public class Task4Entity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<User> Users { get; set; }
    }
}
